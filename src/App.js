import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Attendance from "./components/Attendance";

function App() {
	return (
		<div className="App">
			<Attendance title="Attendances code" />
		</div>
	);
}

export default App;
