import React, { useState, useEffect } from 'react';
import ReactTable from 'react-table-v6';
import 'react-table-v6/react-table.css';

const Attendance = ({ title }) => {
  const [attendance, setAttendace] = useState([]);

  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Key',
      accessor: 'key',
    },
  ];

  useEffect(() => {
    fetch('https://duongsemdemo.herokuapp.com/attendances', {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Accept: 'application/json;',
        Authorization: 'Basic' + btoa('admin:admin123'),
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('data: ', data);
        setAttendace(data);
      })
      .catch(err=> console.log('error in fetch data', err))

  }, []);

  return (
    <div>
      <h4>{title}</h4>
      <ReactTable data={attendance} columns={columns} />
    </div>
  );
};

export default Attendance;
